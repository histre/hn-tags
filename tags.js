(() => {
  const $stories = document.querySelectorAll('.athing');
  if ($stories.length <= 0) {
    return;
  }

  const host = 'http://local.histre.com:8080/';
  const ids = [];

  $stories.forEach((story) => {
    ids.push(story.id);
  });

  if (ids.length > 0) {
    let query = '';

    for (const id of ids) {
      query = `${query}${id},`;
    }
    query = query.slice(0, -1);

    chrome.runtime.sendMessage({ action: 'getTags', query }, (result) => {
      if (result.error || !result.data?.stories) {
        // Do error handling
        return;
      }

      const stories = result.data.stories;

      for (const storyId in stories) {
        const suggestedTags = stories[storyId].suggested_tags;

        if (!Array.isArray(suggestedTags)) {
          // Do error handling
          return;
        }

        if (suggestedTags.length > 0) {
          const $storyTitle = document
            .getElementById(storyId)
            ?.querySelector('.title .titleline')
            ?.closest('.title');
          if (!$storyTitle) continue;

          $storyTitle.querySelector('[data-histre-hn-tag-wrap]')?.remove();

          const $wrap = document.createElement('span');
          $wrap.setAttribute('data-histre-hn-tag-wrap', '');

          for (const tag of suggestedTags) {
            const $tag = document.createElement('a');
            $tag.setAttribute('class', 'histre-hn-tag');
            $tag.setAttribute('href', `${host}hn/?tags=+${tag}`);
            $tag.setAttribute('target', '_blank');
            $tag.setAttribute('rel', 'noopener noreferrer');
            $tag.innerText = '#' + tag;
            $wrap.appendChild($tag);
          }

          $storyTitle.appendChild($wrap);
        }
      }
    });
  }
})();
