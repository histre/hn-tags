const host = 'http://local.histre.com:8080/';
const isDevMode = host === 'http://local.histre.com:8080/';
const hnApi = `${host}api/v1/hn/`;

function onMessageHandler(message, sender, sendResponse) {
  if (message.action === 'getTags') {
    fetch(`${hnApi}?stories=${message.query}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'X-Histre-For': 'extn',
      },
      credentials: 'include',
    })
      .then((response) => {
        return response.json();
      })
      .then((result) => {
        sendResponse(result);
      })
      .catch((e) => {
        if (isDevMode) {
          console.error(e);
        }
        sendResponse({ error: true });
      });

    return true;
  }
}

chrome.runtime.onMessage.addListener(onMessageHandler);
